# Описание методов API для работы с профилем пользователя

## GET user?id={id}

__Описание:__

 Метод для получения публичной информации о пользователе из его профиля

__Параметры и данные:__

`id` – уникальный идентификатор пользователя (опциональный параметр, если `null`, то загружается информация для текущего пользователя по его токену)

__Ответ:__

```json
{
    "id":"",
    "photoUrl":"some photo url",
    "nickname":"alesha_karamazov",
    "city":"Санкт-Петербург",
    "socials":
    [
        {
            "socialType":0,
            "socialUrl":"http://host.com"
        }
    ],
    "rating":
    {
        "value":36,
        "upvotes":37,
        "downvotes":1
    }
}
```

`socialType` – тип социальной сети:

    0 – facebook
    1 – vk
    2 – twitter,
    3 - одноклассники

## GET user/lastVisitedMatches?profileId={id}

__Описание:__

 Метод для получения списка матчей посещенных пользователем

__Параметры и данные:__

`profileId` – уникальный идентификатор пользователя

__Ответ:__

```json
[
    {
        "id":"123123",
        "buyTicketsUrl":"url",
        "translationsUrl":"some url",
        "date":"YYYY-MM-DDThh:mm:ss",
        "tournamentName":"РОСГОССТРАХ-Чемпионат России",
        "tour":"30 тур",
        "stadium":"Петровский",
        "status":1,
        "homeTeam":
        {
            "id":"123",
            "name":"Название команды, которая играет дома",
            "abbreviation":"АБР",
            "city":"Название родного города команды",
            "country":"Название родной страны команды",
            "logo":"base64 image"
        },
        "onTheRoadTeam":
        {
            "id":"123",
            "name":"Название команды, которая играет на выезде",
            "abbreviation":"АБР",
            "city":"Название родного города команды",
            "country":"Название родной страны команды",
            "logo":"base64 image"
        },
        "score":
        {
            "homeTeam":
            {
                "goals":3,
                "players":
                [
                    {
                        "minute":"45'",
                        "surname":"Шатов"
                    },
                    {
                        "minute":"45'",
                        "surname":"Халк"
                    },
                    {
                        "minute":"85'",
                        "surname":"Рондон"
                    }
                ]
            },
            "onTheRoadTeam":
            {
                "goals":0,
                "players":null
            }
        },
        "videoBroadcastUrl":"some_broadcast_url",
        "radioBroadcastUrl":"some_broadcast_url",
        "isOurTeamHomeMatch":true
    }
]
```