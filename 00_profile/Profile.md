# Описание методов API для работы с профилем

## Оглавление

- [Профиль](User.md)
- [Активность](Activity.md)
- [DEPRETATED – Дисконтная карта](Discount.md)
- [Настройки](Settings.md)
- [Заказы](Orders.md)