# Документация API официального мобильного приложения ФК Зенит

## Оглавление

- [Основные положения](./_base/README.md)
- [Aвторизация и работа с профилем пользователя](./00_profile/README.md)
- [Главный экран (сводка новостей и событий)](./01_main/README.md)
- [История уведомлений](./02_notifications/README.md)
- [Билеты](./03_tickets/README.md)
- [Магазин](./04_shop/README.md)
- [Матчи](./05_matches/README.md)
- [Новости](./06_news/README.md)
- [Команда](./07_team/README.md)
- [Фото/медиа](./08_photos/README.md)
- [Зенит-ТВ](./09_zenit_tv/README.md)
- [Таблицы](./10_tables/README.md)
- [Клуб](./11_club/README.md)
- [Контакты](./12_contacts/README.md)
- [Дисконтная карта](./13_discount_card/README.md)
- [Дополнительные материалы](./14_additional_materials/README.md)
- [Описание типов пуш-уведомлений](./15_push_notifications/README.md)
- [Комментариии](./16_comments/README.md)
- [Описание типов deeplink'ов](./17_deeplinks/README.md)
- [Я на матче](./18_on_the_match/README.md)