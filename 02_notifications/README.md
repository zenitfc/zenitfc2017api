# Описание методов работы с историей уведомлений

## GET notifications_active?offset=0&count=10 и GET notifications_archive

__Описание:__

Загрузка списка уведомлений для пользователя

__Параметры и данные:__

`offset` – обозначает отступ (с какой позиции отдавать уведомления)
`count` – требуемое число элементов в списке в ответе (если в ответе будет меньшее число элементов, то последующие запросы на дозагрузку уведомлений выполняться не будут)

__Ответ:__

```json
[
    {
        "id":"1",
        "publicationDate":"YYYY-MM-DDThh:mm:ss",
        "previewPhotoUrl":"http://host.com/some_photo_url",
        "title":"Заголовок новости",
        "deeplink":"survey://id=1",
        "webUrl":"http://external.resource.com/some_news_url",
        "htmlContent":"<html></html>"
    }
]
```

## GET notification?id={id}

__Описание:__

Загрузка подробной информации об уведомлении

__Параметры и данные:__

`id` – идентификатор уведомления

__Ответ:__

```json
{
    "id":"1",
    "publicationDate":"YYYY-MM-DDThh:mm:ss",
    "previewPhotoUrl":"http://host.com/some_photo_url",
    "title":"Заголовок новости",
    "deeplink":"survey://id=1",
    "webUrl":"http://external.resource.com/some_news_url",
    "htmlContent":"<html></html>"
}
```