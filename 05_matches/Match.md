# Метода получения подробной информации о матче

## GET match/events?id=123123

__Описание:__

Загрузка списка событий, произошедших в течении матча

__Параметры и данные:__

Параметры и данные отсутствуют

__Ответ:__

```json
[
    {
        "id":"1",
        "teamType":0,
        "minute":45,
        "additionalMinutes":3,
        "icon":"base64 string",
        "text":"Уходим на перерыв. 2:0!",
        "instagramPhotoUrl":"",
        "instagramAuthor":"",
        "showOnTimeline":true,
        "isGoal":true
    }
]
```

`teamType` – тип команды, у которой произошло событие, может принимать значения:

    0 – команда, играющая дома
    1 – команда, играющая на выезде

## GET match?id=123123

__Описание:__

Загрузка краткой информации о матче. Используется при переходе в карточку матча по deeplink

__Параметры и данные:__

`id` – идентификатор матча

__Ответ:__

```json
{
    "id":"123123",
    "buyTicketsUrl":"some url",
    "translationsUrl":"some url",
    "date":"YYYY-MM-DDThh:mm:ss",
    "tournamentName":"РОСГОССТРАХ-Чемпионат России",
    "tour":"30 тур",
    "stadium":"Петровский",
    "status":1,
    "homeTeam":
    {
        "id":"123",
        "name":"Название команды, которая играет дома",
        "city":"Название родного города команды",
        "country":"Название родной страны команды",
        "logo":"base64 image"
    },
    "onTheRoadTeam":
    {
        "id":"123",
        "name":"Название команды, которая играет на выезде",
        "city":"Название родного города команды",
        "country":"Название родной страны команды",
        "logo":"base64 image"
    },
    "score":
    {
        "homeTeam":
        {
            "goals":3,
            "players":
            [
                {
                    "minute":"45'",
                    "surname":"Шатов"
                },
                {
                    "minute":"45'",
                    "surname":"Халк"
                },
                {
                    "minute":"85'",
                    "surname":"Рондон"
                }
            ]
        },
        "onTheRoadTeam":
        {
            "goals":0,
            "players":null
        }
    },
    "videoBroadcastUrl":"some_broadcast_url",
    "radioBroadcastUrl":"some_broadcast_url"
}
```

`status` – обозначает статус матча, может принимать значения:

    0 – матч до начала
    1 – матч начался
    2 – матч закончился