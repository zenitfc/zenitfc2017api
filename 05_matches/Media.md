# Описание методов работы с разделом Медиа

## GET media/photo?id=1

__Описание:__

Загрузка списка фотографий отснятых в течении матча

__Параметры и данные:__

`id` – идентификатор матча
`playerId` – идентификатор игрока, сужает выборку до материалов по одному игроку
`coachId` – аналогичен `playerId`, но для тренера

__Ответ:__

```json
[
    {
        "id":"1",
        "previewPhotoUrl":"some photo url",
        "publicationDate":"YYY-MM-DDThh:mm:ss",
        "title":"«Зенит» — «Арсенал»: конкурс предматчевых афиш",
        "imageUrl":"some image url"
    }
]
```

## GET media/video?id=1

Загрузка списка видеоальбомов, отснятых в течении матча

__Параметры и данные:__

`id` – идентификатор матча
`playerId` – идентификатор игрока, сужает выборку до материалов по одному игроку
`coachId` – аналогичен `playerId`, но для тренера

__Ответ:__

```json
[
    {
        "id":"1",
        "previewPhotoUrl":"some photo url",
        "publicationDate":"YYY-MM-DDThh:mm:ss",
        "title":"«Зенит» — «Арсенал»: конкурс предматчевых афиш",
        "videoUrl":"some video url"
    }
]
```

## GET media/photo_albums?galleryId=123&offset=0&count=10

__Описание:__

Загрузка списка фотоальбомов/фотоновостей по команде

__Параметры и данные:__

`galleryId` – идентификатор команды

__Ответ:__

```json
[
    {
        "id":"1",
        "commentsId":"123",
        "previewPhotoUrl":"some photo url",
        "publicationDate":"YYY-MM-DDThh:mm:ss",
        "title":"«Зенит» — «Арсенал»: конкурс предматчевых афиш",
        "photosCount":45,
        "commentsCount":0
    }
]
```

## GET media/video_albums?galleryId=123&offset=0&count=10

__Описание:__

Загрузка списка видеоальбомов/видеоновостей по команде

__Параметры и данные:__

`galleryId` – идентификатор команды

__Ответ:__

```json
[
    {
        "id":"1",
        "commentsId":"123",
        "previewPhotoUrl":"some photo url",
        "publicationDate":"YYY-MM-DDThh:mm:ss",
        "title":"«Зенит» — «Арсенал»: конкурс предматчевых афиш",
        "photosCount":45,
        "commentsCount":0,
        "duration":"1:56"
    }
]
```