# Описание методов работы АПИ с составами команд

## GET team?matchId=123&teamId=123

__Описание:__

Загрузка информации о составе команды

__Параметры и данные:__

`matchId` – идентификатор матча
`teamId` - идентификатор команды по которой требуется получить информацию

__Ответ:__

```json
[
    {
        "groupName":"Основной состав",
        "players":
        [
            {
                "id":"0",
                "number":1,
                "fullName":"Юрий Лодыгин",
                "age":26,
                "birthdate":"YYYY-MM-DD",
                "roleName":"Вратарь",
                "roleIcon":"base64 string",
                "events":
                [
                    {
                        "id":"1",
                        "minute":45,
                        "additionalMinute":3,
                        "icon":"base64 string"
                    }
                ],
                "isOurTeamPlayer":true
            }
        ],
    },
    {
        "groupName":"Запасные",
        "players":
        [
            {
                "id":"0",
                "number":1,
                "fullName":"Юрий Лодыгин",
                "age":26,
                "birthdate":"YYYY-MM-DD",
                "roleName":"Вратарь",
                "roleIcon":"base64 string",
                "events":
                [
                    {
                        "id":"1",
                        "minute":45,
                        "additionalMinute":3,
                        "icon":"base64 string"
                    }
                ],
                "isOurTeamPlayer":true
            }
        ]
    }
]
```

## GET team_best?id=123

__Описание:__

Загрузка претендентов на лучшего игрока матча

__Параметры и данные:__

`id` – идентификатор матча

__Ответ:__

```json
[
    {
        "id":"0",
        "number":1,
        "fullName":"Юрий Лодыгин",
        "age":26,
        "birthdate":"YYYY-MM-DD",
        "roleName":"Вратарь",
        "icon":"base64 string",
        "events":
        [
            {
                "id":"1",
                "minute":45,
                "additionalMinute":3,
                "icon":"base64 string"
            }
        ],
        "userRating":null
    }
]
```

## POST team_best

__Описание:__

Отправка результатов голосования пользователя за лучшего игрока матча

__Параметры и данные:__

```json
{
    "id":"123", //идентификатор матча
    "answers":
    [
        {
            "id":"0",  //идентификатор игрока
            "userRating":5
        }
    ]
}
```

__Ответ:__

В случае успешной отправки данных сервер возвращает `200 OK`

## GET team_best_player?id=123

__Описание:__

Загрузка информации о лучшем игроке матча

__Параметры и данные:__

`id` – идентификатор матча

__Ответ:__

```json
{
    "id":"0",
    "number":1,
    "fullName":"Юрий Лодыгин",
    "photoUrl":"",
    "averageUserRating":4.5
}
```