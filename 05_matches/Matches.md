# Методы для получения списка матчей и информации о ближайшем матче

## GET coming_matches

__Описание:__

Загрузка информации о ближайших матчах Зенита

__Параметры и данные:__

Параметры и данные отсутствуют

__Ответ:__

```json
{
    "defaultDisplayedMatchIndex":0,
    "matches":
    [
        {
            "id":"12321",
            "buyTicketsUrl":"url",
            "translationsUrl":"some url",
            "date":"YYYY-MM-DDThh:mm:ss",
            "tournamentName":"РОСГОССТРАХ-Чемпионат России",
            "tour":"30 тур",
            "stadium":"Петровский",
            "status":1,
            "homeTeam":
            {
                "id":"123",
                "name":"Название команды, которая играет дома",
                "abbreviation":"АБР",
                "city":"Название родного города команды",
                "country":"Название родной страны команды",
                "logo":"base64 image"
            },
            "onTheRoadTeam":
            {
                "id":"124",
                "name":"Название команды, которая играет на выезде",
                "abbreviation":"АБР",
                "city":"Название родного города команды",
                "country":"Название родной страны команды",
                "logo":"base64 image"
            },
            "score":
            {
                "homeTeam":
                {
                    "goals":3,
                    "players":
                    [
                        {
                            "minute":"45'",
                            "surname":"Шатов"
                        },
                        {
                            "minute":"45'",
                            "surname":"Халк"
                        },
                        {
                            "minute":"85'",
                            "surname":"Рондон"
                        }
                    ]
                },
                "onTheRoadTeam":
                {
                    "goals":0,
                    "players":null
                }
            },
            "videoBroadcastUrl":"some_broadcast_url",
            "radioBroadcastUrl":"some_broadcast_url",
            "isOurTeamHomeMatch":true
        }
    ]
}
```

## GET matches_teams

__Описание:__

Загрузка списка команд по которым можно просмотреть расписание

__Параметры и данные:__

Параметры и данные отсутствуют

__Ответ:__

```json
[
    {
        "id":"123",
        "name":"Название команды",
    },
    {
        "id":"123",
        "name":"Название команды"
    }
]
```

## GET team/seasons?id=123

__Описание:__

Загрузка списка игровых сезонов команды

__Параметры и данные:__

`id` – идентификатор команды

__Ответ:__

```json
[
    {
        "id":"123",
        "name":"2016/2017",
    },
    {
        "id":"123",
        "name":"2015/2016"
    }
]
```

## GET matches_team?id=123&season_id=0

__Описание:__

Загрузка списка матчей команды в определенном сезоне

__Параметры и данные:__

`id` – идентификатор команды
`season_id` – идентификатор сезона, если 0, то требуется информация по текущему (последнему) сезону

__Ответ:__

```json
[
    {
        "id":"123123",
        "buyTicketsUrl":"url",
        "translationsUrl":"some url",
        "date":"YYYY-MM-DDThh:mm:ss",
        "tournamentName":"РОСГОССТРАХ-Чемпионат России",
        "tour":"30 тур",
        "stadium":"Петровский",
        "status":1,
        "homeTeam":
        {
            "id":"123",
            "name":"Название команды, которая играет дома",
            "abbreviation":"АБР",
            "city":"Название родного города команды",
            "country":"Название родной страны команды",
            "logo":"base64 image"
        },
        "onTheRoadTeam":
        {
            "id":"123",
            "name":"Название команды, которая играет на выезде",
            "abbreviation":"АБР",
            "city":"Название родного города команды",
            "country":"Название родной страны команды",
            "logo":"base64 image"
        },
        "score":
        {
            "homeTeam":
            {
                "goals":3,
                "players":
                [
                    {
                        "minute":"45'",
                        "surname":"Шатов"
                    },
                    {
                        "minute":"45'",
                        "surname":"Халк"
                    },
                    {
                        "minute":"85'",
                        "surname":"Рондон"
                    }
                ]
            },
            "onTheRoadTeam":
            {
                "goals":0,
                "players":null
            }
        },
        "videoBroadcastUrl":"some_broadcast_url",
        "radioBroadcastUrl":"some_broadcast_url",
        "isOurTeamHomeMatch":true
    }
]
```

`status` – обозначает статус матча, может принимать значения:

    0 – матч до начала
    1 – матч начался
    2 – матч закончился

## GET matches_parallels?id=123123

__Описание:__

Загрузка списка матчей параллельно идущих с выбранным в том же туре (стадии турнира)

__Параметры и данные:__

`id` – идентификатор матча

__Ответ:__

Ответ аналогичен ответу на запрос `GET matches_team`