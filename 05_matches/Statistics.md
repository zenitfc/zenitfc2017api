# Описание методов работы со статистикой матча

## GET statistics?id=123

__Описание:__

Загрузка статистики по матчу

__Параметры и данные:__

`id` – идентификатор матча

__Ответ:__

```json
[
    {
        "id":"0",
        "categoryName":"Общая статистика",
        "parameters":
        [
            {
                "id":"1",
                "title":"Владение мячом",
                "homeTeam":
                {
                    "value":"61",
                    "percent":61.0
                },
                "onTheRoadTeam":
                {
                    "value":"39",
                    "percent":39.0
                },
            }
        ]
    },
    {
        "id":"1",
        "categoryName":"Атака",
        "parameters":
        [
            {
                "id":"1",
                "title":"Голы",
                "homeTeam":
                {
                    "value":"1",
                    "percent":100.0
                },
                "onTheRoadTeam":
                {
                    "value":"0",
                    "percent":0.0
                },
            }
        ]
    }
]
```