# Описание методов работы с контентом из соц. сетей

## GET social_availability?id=123

__Описание:__

 Метод для определения доступности раздела для конкретного матча

__Параметры и данные:__

`id` – идентификатор матча

__Ответ:__

```json
{
    "isAvailable":true
}
```

## GET socials?id=123&social_type=0&offset=0&count=10

__Описание:__

 Метод для загрузки контента из Instagram

__Параметры и данные:__

`id` – идентификатор матча
`social_type` – тип социальной сети, может принимать значения:

    0 – Instagram
    1 – Twitter
    2 – Swarm

__Ответ:__

```json
[
    {
        "id":"1",
        "userAvatar":"some photo url",
        "photoUrl":"some photo url",
        "username":"@alesha_karamazov",
        "userFullName":"Алексей Карамазов",
        "place":"Стадион Зенита",
        "publicationDate":"YYYY-MM-DDThh:mm:ss",
        "text":"#зенит #динамозенит #спб #выезд #химки#spb #Zenit #dinamo #dinamozenit #saintp"
    }
]
```