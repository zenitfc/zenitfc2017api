# Описание методов АПИ для работы с разделом Зенит-ТВ

## GET media/video_categories

__Описание:__

Загрузка списка категорий

__Параметры и данные:__

Параметры и данные отсутствуют

__Ответ:__

```json
[
    {
        "id":"123",
        "name":"Название категории",
    },
    {
        "id":"123",
        "name":"Название категории"
    }
]
```

## GET media/video?galleryId=123

[См. методы раздела Медиа](../05_matches/Media.md)

## GET media/video_content?id=1

__Описание:__

Загрузка информации о новости с видео

__Параметры и данные:__

`id` – идентификатор новости/видео

__Ответ:__

```json
{
    "description":"19 мая 2015. Россия, Санкт-Петербург, стадион «Петровский». Автограф-сессия с вратарём ФК «Зенит» Юрием Лодыгиным (на снимке справа).",
    "autor":"Автор: Вячеслав Евдом",
    "tags":
    [
        {
            "title":"Юрий Лодыгин",
            "deeplink":"player://id=1"
        },
        {
            "title":"Чемпионат России",
            "url":"http://host.com"
        }
    ],
    "videoUrl":"some video url"
}
```

`videoUrl` – пример: `http://fc-zenit.ru/video/42853/?fullscreen=Y&blocks=1&video_id=zaw46xmZjVBQgk4EevY2QKrJ9`

## GET media/full_video_content?id=1

__Описание:__

Загрузка полной информации о новости с видео

__Параметры и данные:__

`id` – идентификатор новости/видео

__Ответ:__

```json
{
    "id":"1",
    "commentsId":"123",
    "previewPhotoUrl":"some photo url",
    "publicationDate":"YYY-MM-DDThh:mm:ss",
    "title":"«Зенит» — «Арсенал»: конкурс предматчевых афиш",
    "photosCount":45,
    "commentsCount":0,
    "duration":"1:56",
    "description":"19 мая 2015. Россия, Санкт-Петербург, стадион «Петровский». Автограф-сессия с вратарём ФК «Зенит» Юрием Лодыгиным (на снимке справа).",
    "autor":"Автор: Вячеслав Евдом",
    "tags":
    [
        {
            "title":"Юрий Лодыгин",
            "deeplink":"player://id=1"
        },
        {
            "title":"Чемпионат России",
            "url":"http://host.com"
        }
    ],
    "videoUrl":"some video url"
}
```

`videoUrl` – пример: `http://fc-zenit.ru/video/42853/?fullscreen=Y&blocks=1&video_id=zaw46xmZjVBQgk4EevY2QKrJ9`

## GET comments/video?id=123&offset=0&count=10

[См. раздел Комментарии](../16_comments/README.md)

## POST comments/new

[См. раздел Комментарии](../16_comments/README.md)