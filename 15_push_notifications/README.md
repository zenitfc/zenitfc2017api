# Описание методов и данных API для push уведомлений

## Оглавление

- [Описание типов уведомлений](Notifications_types.md)
- [Описание методов регистрации на пуш уведомления](Notifications_methods.md)