# Описание типов push уведомлений

## Общий пуш

```json
{
    "aps":
    {
        "alert":{"title":"Testing...","subtitle":"Push notification from Zenit","body":"This is the message body of the notification"},
        "badge":1,
        "sound":"default"
    },
    "deeplink":"news://id=1"
}
```

## О событиях матча

На устройства подтвердившие согласие на получение пуш-уведомления должен рассылаться пуш вида:

```json
{
    "aps":
    {
        "alert":{"title":"Testing...","subtitle":"Push notification from Zenit","body":"This is the message body of the notification"},
        "badge":1,
        "sound":"default"
    },
    "deeplink":"match://id=1&popup=true"
}
```

При получении/открытии уведомления такого вида приложение отображает соответствующий диалог с информацией и производит дозагрузку событий в матче по его `id`

## О наличии доступного опроса (анкеты)

На устройства подтвердившие согласие на получение пуш-уведомления должен рассылаться пуш вида:

```json
{
    "aps":
    {
        "alert":{"title":"Testing...","subtitle":"Push notification from Zenit","body":"This is the message body of the notification"},
        "badge":1,
        "sound":"default"
    },
    "deeplink":"quiz://id=1&popup=true"
}
```

При получении/открытии уведомления такого вида приложение поверх текущего экрана открывает поп-ап с опросом.