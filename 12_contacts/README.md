# Описание методов API для работы с Контактами

## GET contacts/stadium

__Описание:__

Получение информации о стадионе

__Параметры и данные:__

Параметры и данные отсутствуют

__Ответ:__

```json
{
    "title":"Стадион Петровский",
    "address":"",
    "phone":"",
    "wortTime":"",
    "email":"",
    "photoPreviewUrl":"some photo preview"
}
```

## GET contacts/office

__Описание:__

Получение информации об офисе

__Параметры и данные:__

Параметры и данные отсутствуют

__Ответ:__

```json
{
    "title":"Стадион Петровский",
    "address":"",
    "phone":"",
    "wortTime":"",
    "email":"",
    "photoPreviewUrl":"some photo preview"
}
```