# Описание методов API для работы с разделом Таблицы

## GET teams

[См. получение списка доступных команд](../07_team/Players.md)

## GET team/seasons?id=123

[См. получение списка игровых сезонов команды](../05_matches/Matches.md)

## GET championships?teamId=1&seasonId=123

__Описание:__

Загрузка списка турниров для конкретной команды

__Параметры и данные:__

`teamId` - идентификатор команды

__Ответ:__

```json
[
    {
        "id":"1",
        "name":"Чемпионат России",
        "isRounded":true
    }
]
```

## GET championships?teamId=1&champId=1&seasonId=1

__Описание:__

Загрузка результатов

__Параметры и данные:__

`teamId` - идентификатор команды
`champId` – идентификатор чемпионата
`seasonId` – идентификатор сезона

__Ответ:__

```json
[
    {
        "stageType":0,
        "group":
        {
            "title":"ГРУППА С",
            "teams":
            [
                {
                    "id":"1",
                    "position":"1",
                    "icon":"base64 string",
                    "name":"Зенит",
                    "matches":17,
                    "goalsScoredAndConceded":3,
                    "points":41,
                    "wins":17,
                    "draws":31,
                    "defeats":41,
                    "goalsScored":17,
                    "goalsConceded":20
                }
            ]
        },
        "matches":
        [
            {
                "tourName":"1/8 ФИНАЛА",
                "matches":
                [
                    {
                        "id":"123123",
                        "date":"YYYY-MM-DDThh:mm:ss",
                        "tournamentName":"РОСГОССТРАХ-Чемпионат России",
                        "tour":"30 тур",
                        "stadium":"Петровский",
                        "status":1,
                        "homeTeam":
                        {
                            "id":"123",
                            "name":"Название команды, которая играет дома",
                            "city":"Название родного города команды",
                            "country":"Название родной страны команды",
                            "logo":"base64 image"
                        },
                        "onTheRoadTeam":
                        {
                            "id":"123",
                            "name":"Название команды, которая играет на выезде",
                            "city":"Название родного города команды",
                            "country":"Название родной страны команды",
                            "logo":"base64 image"
                        },
                        "score":
                        {
                            "homeTeam":
                            {
                                "goals":3,
                                "players":null
                            },
                            "onTheRoadTeam":
                            {
                                "goals":0,
                                "players":null
                            }
                        },
                        "videoBroadcastUrl":"some_broadcast_url",
                        "radioBroadcastUrl":"some_broadcast_url"
                    }
                ]
            }
        ]
    }
]
```

`stageType` – принимает значения:
    0 – таблица результатов (должно быть заполнено поле `group`)
    1 – список матчей (должно быть заполнено поле `matches`)

Если чемпионат круговой, то `title` у `group` равен `null`

## GET championships/grid?teamId=1&champId=1&seasonId=1

__Описание:__

Загрузка сетки матчей по контретному некруговому чемпионату

__Параметры и данные:__

`teamId` - идентификатор команды
`champId` – идентификатор чемпионата
`seasonId` – идентификатор сезона

__Ответ:__

```json
[
    {
        "stageName":"1/8 ФИНАЛА",
        "duels":
        [
            {
                "id":"123123",
                "homeTeam":
                {
                    "id":"123",
                    "name":"Название команды, которая играет дома",
                    "logo":"base64 image"
                },
                "onTheRoadTeam":
                {
                    "id":"123",
                    "name":"Название команды, которая играет на выезде",
                    "logo":"base64 image"
                },
                "withOurTeam":true,
                "results":
                [
                    {
                        "homeTeam":"1",
                        "onTheRoadTeam":"0"
                    },
                    {
                        "homeTeam":"0",
                        "onTheRoadTeam":"1"
                    },
                    {
                        "homeTeam":"10 (2)",
                        "onTheRoadTeam":"1 (3)"
                    }
                ]
            }
        ]
    }
]
```

`withOurTeam` – флаг для индикации поединков с участием Зенита
`results` – результаты двух матчей + итоговое количество очков