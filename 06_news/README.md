# Описание методов API для работы с новостями

## GET news/categories

__Описание:__

Загрузка списка категорий новостей

__Параметры и данные:__

Параметры и данные отсутствуют

__Ответ:__

```json
[
    {
        "id":"123",
        "name":"Название категории",
    },
    {
        "id":"123",
        "name":"Название категории"
    }
]
```

## GET news?id=0&offset=0&count=10

__Описание:__

Загрузка списка актуальных новостей

__Параметры и данные:__

`id` – идентификатор категории новостей
`offset` – обозначает отступ (с какой позиции отдавать новости)
`count` – требуемое число элементов в списке в ответе (если в ответе будет меньшее число элементов, то последующие запросы на дозагрузку новостей выполняться не будут)

__Ответ:__

```json
[
    {
        "id":"1",
        "galleryId":"123",
        "commentsId":"123",
        "type":0,
        "glyph":"base64 string",
        "publicationDate":"YYYY-MM-DDThh:mm:ss",
        "categoryName":"название категории",
        "previewPhotoUrl":"http://host.com/some_photo_url",
        "title":"Заголовок новости",
        "description":"Описание новости",
        "webUrl":"http://external.resource.com/some_news_url",
        "author":"автор материала"
    }
]
```

`type` – тип новости, принимает значения:

    0 – текстовая новость
    1 – новость с видео
    2 – новость с фотогаллереей
    3 – новость с аудио
    4 – новость с переходом по ссылке на внешний источник
    5 – новость из блога

`glyph` – глиф – небольшое изображение (иконка), отображаемая на новости, в формате base64

## GET news/text_content?id=1

__Описание:__

Загрузка контента текстовой новости

__Параметры и данные:__

`id` – идентификатор новости

__Ответ:__

```json
{
    "htmlContent":"<html></html>",
    "contentPreviewPhotoUrl":"http://host.com/some_photo_url",
    "contentPreviewPhotoUrlAuthor":"Фото: отличного фотографа"
}
```

## GET news/full_content?id=1

__Описание:__

Загрузка всей информации о текстовой новости

__Параметры и данные:__

`id` – идентификатор новости

__Ответ:__

```json
{
    "id":"1",
    "type":0,
    "commentsId":"123",
    "glyph":"base64 string",
    "publicationDate":"YYYY-MM-DDThh:mm:ss",
    "categoryName":"название категории",
    "previewPhotoUrl":"http://host.com/some_photo_url",
    "title":"Заголовок новости",
    "description":"Описание новости",
    "webUrl":"http://external.resource.com/some_news_url",
    "author":"автор материала",
    "htmlContent":"<html></html>",
    "contentPreviewPhotoUrl":"http://host.com/some_photo_url",
    "contentPreviewPhotoUrlAuthor":"Фото: отличного фотографа"
}
```

## GET news?id=0&offset=0&count=10&personId=1

__Описание:__

Загрузка списка новостей по конкретному игроку

__Параметры и данные:__

`id` – идентификатор категории новостей
`offset` – обозначает отступ (с какой позиции отдавать новости)
`count` – требуемое число элементов в списке в ответе (если в ответе будет меньшее число элементов, то последующие запросы на дозагрузку новостей выполняться не будут)
`personId` – идентификатор игрока

__Ответ:__

```json
[
    {
        "id":"1",
        "galleryId":"123",
        "type":0,
        "glyph":"base64 string",
        "publicationDate":"YYYY-MM-DDThh:mm:ss",
        "categoryName":"название категории",
        "previewPhotoUrl":"http://host.com/some_photo_url",
        "title":"Заголовок новости",
        "webUrl":"http://external.resource.com/some_news_url"
    }
]
```

`type` – тип новости, принимает значения:

    0 – текстовая новость
    1 – новость с видео
    2 – новость с фотогаллереей
    3 – новость с аудио
    4 – новость с переходом по ссылке на внешний источник
    5 – новость из блога

`glyph` – глиф – небольшое изображение (иконка), отображаемая на новости, в формате base64

## GET comments/news?id=123&offset=0&count=10

[См. методы работы с комментариями](../16_comments/README.md)