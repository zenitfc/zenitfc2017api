# Описание методов работы с баннерами

## GET start_banner

__Описание:__

Загрузка рекламного изображения (баннера) при старте приложения

__Параметры и данные:__

Параметры и данные отсутствуют

__Ответ:__

```json
{
    "url":"http://host.com/image_url"
}
```

## GET main_banners

__Описание:__

Загрузка изображений (баннеров) для отображения на главном экране приложения

__Параметры и данные:__

Параметры и данные отсутствуют

__Ответ:__

```json
[
    {
        "id":"banner1",
        "imageUrl":"https://host.com/banner1.png",
        "deeplink":"news://id=1",
        "title":"Заголовок баннера",
        "description":"Описание баннера",
        "links":
        [
            {
                "icon":"base64 image",
                "text":"Узнать больше",
                "deeplink":"news://id=1",
                "externalLink":null
            },
            {
                "icon":"base64 image",
                "text":"Русфонд",
                "deeplink":null,
                "externalLink":"https://www.rusfond.ru/letter/42/13792"
            }
        ]
    }
]
```
