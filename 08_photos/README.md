# Описание методов АПИ для работы с разделом Фото

## GET media/photo_categories

__Описание:__

Загрузка списка категорий

__Параметры и данные:__

Параметры и данные отсутствуют

__Ответ:__

```json
[
    {
        "id":"123",
        "name":"Название категории",
    },
    {
        "id":"123",
        "name":"Название категории"
    }
]
```

## GET media/photo?galleryId=123&offset=0&count=10

[См. методы раздела Медиа](../05_matches/Media.md)

## GET media/photo_content?id=1

__Описание:__

Загрузка списка фотографий с информацией о фотоальбоме

__Параметры и данные:__

`id` – идентификатор фотоальбома (фото-галереи)

__Ответ:__

```json
{
    "description":"19 мая 2015. Россия, Санкт-Петербург, стадион «Петровский». Автограф-сессия с вратарём ФК «Зенит» Юрием Лодыгиным (на снимке справа).",
    "autor":"Автор: Вячеслав Евдом",
    "tags":
    [
        {
            "title":"Юрий Лодыгин",
            "deeplink":"player://id=1"
        },
        {
            "title":"Чемпионат России",
            "url":"http://host.com"
        }
    ],
    "photos":
    [
        {
            "imageUrl":"some image url"
        }
    ]
}
```

## GET media/full_photo_content?id=1

__Описание:__

Загрузка всей информации о фотоальбоме

__Параметры и данные:__

`id` – идентификатор фотоальбома

__Ответ:__

```json
{
    "id":"1",
    "commentsId":"123",
    "publicationDate":"YYYY-MM-DDThh:mm:ss",
    "categoryName":"название категории",
    "previewPhotoUrl":"http://host.com/some_photo_url",
    "title":"Заголовок новости",
    "viewsCount":2901,
    "commentsCount":0,
    "description":"19 мая 2015. Россия, Санкт-Петербург, стадион «Петровский». Автограф-сессия с вратарём ФК «Зенит» Юрием Лодыгиным (на снимке справа).",
    "autor":"Автор: Вячеслав Евдом",
    "tags":
    [
        {
            "title":"Юрий Лодыгин",
            "deeplink":"player://id=1"
        },
        {
            "title":"Чемпионат России",
            "url":"http://host.com"
        }
    ],
    "photos":
    [
        {
            "imageUrl":"some image url"
        }
    ]
}
```

## GET comments/photoalbum?id=123&offset=0&count=10

[См. раздел Комментарии](../16_comments/README.md)

## POST comments/new

[См. раздел Комментарии](../16_comments/README.md)